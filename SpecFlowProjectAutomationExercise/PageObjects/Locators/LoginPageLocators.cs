﻿using OpenQA.Selenium;

namespace SpecFlowProjectAutomationExercise.PageObjects.Locators
{
    public static class LoginPageLocators
    {
        public static class Button
        {
            public static By Login => By.XPath("//button[@type='submit'][contains(.,'Login')]");
        }

        public static class TextField
        {
            public static By Password => By.Name("password");
            public static By Username => By.XPath("//input[@name='username']");
            //private IWebElement FirstNumberElement => _webDriver.FindElement(By.Id("first-number"));
        }

        //private IWebElement FirstNumberElement => _webDriver.FindElement(By.Id("first-number"));
    }
}