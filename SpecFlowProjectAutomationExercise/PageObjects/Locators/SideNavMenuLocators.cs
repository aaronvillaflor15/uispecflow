﻿using OpenQA.Selenium;

namespace SpecFlowProjectAutomationExercise.PageObjects.Locators
{
    public static class SideNavMenuLocators
    {
        public static class NavMenu
        {
            public static By PIM => By.XPath("//span[contains(.,'PIM')]");
        }
    }
}