﻿using OpenQA.Selenium;

namespace SpecFlowProjectAutomationExercise.PageObjects.Locators
{
    public static class DashboardPageLocators
    {
        public static class Header
        {
            public static By Dashboard => By.XPath("//h6[contains(.,'Dashboard')]");
        }

        public static class SideMenuBar
        {
            public static By PIM => By.XPath("//span[contains(.,'PIM')]");
        }
    }
}