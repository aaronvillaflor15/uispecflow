﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using SpecFlowProjectAutomationExercise.PageObjects.Locators;

namespace SpecFlowProjectAutomationExercise.PageObjects.Controllers

{
    /// <summary>
    /// Orangehrm Page Object
    /// </summary>
    ///

    public class LoginPage

    {
        //The URL of the orangehrm to be opened in the browser
        private const string OrangeHrmUrlHomePage = "https://opensource-demo.orangehrmlive.com";

        private const string OrangeHrmUrlLoginPage = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";

        //The Selenium web driver to automate the browser
        private readonly IWebDriver _webDriver;

        public LoginPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public void EnterUsername(string username)
        {
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            _webDriver.FindElement(LoginPageLocators.TextField.Username).SendKeys(username);
        }

        public void EnterPassword(string password)
        {
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            _webDriver.FindElement(LoginPageLocators.TextField.Password).SendKeys(password);
        }

        public void ClickLoginButton()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementToBeClickable(LoginPageLocators.Button.Login));
            _webDriver.FindElement(LoginPageLocators.Button.Login).Click();
        }

        public void IsDashboardVisible()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(DashboardPageLocators.Header.Dashboard));
        }

        public void EnsureOrangehrmIsOpen()
        {
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            if (_webDriver.Url != OrangeHrmUrlHomePage)
            {
                _webDriver.Url = OrangeHrmUrlHomePage;
                _webDriver.Manage().Window.Maximize();
            }
        }

        public void GotoLoginPage()
        {
            _webDriver.Url = OrangeHrmUrlLoginPage;
            _webDriver.Manage().Window.Maximize();
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(LoginPageLocators.TextField.Username));
            wait.Until(ExpectedConditions.ElementIsVisible(LoginPageLocators.TextField.Password));
        }

        public void LoginUser(string username, string password)
        {
            EnterUsername(username);
            EnterPassword(password);
            ClickLoginButton();
        }
    }
}