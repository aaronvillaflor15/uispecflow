﻿using NUnit.Framework;
using OpenQA.Selenium;
using SpecFlowProjectAutomationExercise.Drivers;
using SpecFlowProjectAutomationExercise.PageObjects.Controllers;

namespace SpecFlowProjectAutomationExercise.Hooks
{
    [Binding]
    [Parallelizable(ParallelScope.Fixtures)]
    public class BaseTest
    {
        public static IWebDriver? driver;

        [BeforeScenario]
        public static void BeforeScenario(BrowserDriver browserDriver)
        {
            var orangehrmPageObject = new LoginPage(browserDriver.Current);
            orangehrmPageObject.EnsureOrangehrmIsOpen();
        }

        [AfterScenario]
        public static void AfterScenario()
        {
            //
        }
    }
}